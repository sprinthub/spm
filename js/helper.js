$(document).ready(function () {
  
  'use strict';
  
   var top = 0;
   var scrollBarPos = 0;
   var navbar = $('nav');

   $(window).scroll(function () {
      var scrollBarPos = $(window).scrollTop();
      var navHeight = navbar.height();
      var navImg = navbar.children('a').children('img')[0];
     
      if (top < scrollBarPos && scrollBarPos > navHeight + navHeight) {
        navbar.removeClass('navbar-light');
        navbar.addClass('affix navbar-dark');
        navImg.src = 'images/logo-white.svg';
      } else if (top > scrollBarPos && !(scrollBarPos <= navHeight)) {
        navbar.removeClass('affix navbar-dark');
        navbar.addClass('navbar-light');
        navImg.src = 'images/logo.svg';
      }
      else if (scrollBarPos <= navHeight + navHeight) {
        navbar.removeClass('navbar-light')
        navbar.addClass('navbar-dark');
        navImg.src = 'images/logo-white.svg';
      }
      top = scrollBarPos;
  });
  
});