$(document).ready(function () {
  
  'use strict';
  
   var top = 0;
   var scrollBarPos = 0;
   var navbar = $('nav');

   $(window).scroll(function () {
      var scrollBarPos = $(window).scrollTop();
      var navHeight = navbar.height();
     
      if (top < scrollBarPos && scrollBarPos > navHeight + navHeight) {
        navbar.addClass('affix');
      } else if (top > scrollBarPos && !(scrollBarPos <= navHeight)) {
        navbar.removeClass('affix');
      }
      top = scrollBarPos;
  });

  $('.projects').hover((elem) => {
    var node = $(elem.currentTarget);
    node.children('div').css('opacity', 1);
    node.children('img').css('transform', 'scale(1.5)');
    node.children('div').addClass('w3-container w3-center w3-animate-bottom');
  }, (elem) => {
    var node = $(elem.currentTarget);
    node.children('div').removeClass('w3-container w3-center w3-animate-bottom');
    node.children('img').css('transform', 'scale(1)');
    node.children('div').css('opacity', 0);
  });
  
  $('.card-link').hover((elem) => {
    var node = $(elem.currentTarget);
    node.children('i').css('animation', 'shake 2s infinite');
    }, (elem) => {
      var node = $(elem.currentTarget);
      node.children('i').css('animation', 'shake');
  });
  
});